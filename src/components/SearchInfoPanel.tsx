import React, { ReactNode } from 'react';
import styled from 'styled-components';
import { BlueText } from './CommonStyles';

interface SearchInfoPanelProps {
  title: string;
  children: ReactNode;
}

export const SearchInfoPanel = ({ title, children }: SearchInfoPanelProps) => {
  return (
    <Container>
      <Title>{title}</Title>
      <BlueText style={{ padding: '16px' }}>{children}</BlueText>
    </Container>
  );
};

const Title = styled.div`
  background-color: #f5f5f5;
  color: #f26522 !important;
  padding: 10px 12px;
  border-radius: 4px;
  font-size: 16px;
  font-weight: 600;
`;

const Container = styled.div`
  width: 340px;
`;
