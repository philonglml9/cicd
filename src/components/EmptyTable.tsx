import React from 'react';
import styled from 'styled-components';
import { ReactComponent as LogoImg } from '../assets/empty_table.svg';

export const EmptyTableContent = () => {
  return (
    <Container>
      <LogoImg data-testid="empty-svg" />
      <NoDataText>Không có dữ liệu hiển thị</NoDataText>
    </Container>
  );
};

const Container = styled.div`
  svg {
    width: 30%;
    height: 30%;
  }
`;

const NoDataText = styled.p`
  margin-top: 1rem;
  font-size: 17px;
  color: #00467f;
  font-weight: 550;
`;
