import {
  AddAddressWarehousePayload,
  AdminAddDriverPayload,
  AdminAddStockKeeperPayload,
  ChangePasswordPayload,
  DriverInterface,
  GetListDriverResponse,
  GetListStockerResponse,
  GetTransportationCostPayload,
  GetTransportationCostResponse,
  PaymentPayload,
  PaymentResponse,
  SignInForm,
  SignUpPayload,
  UpdateInfoUserPayload,
  UserInterface,
  Vehicle,
  WareHouse,
  WarehouseDriver,
} from '../interface/Auth';
import { getAccessToken, setAccessToken } from '../utils/accessToken';
import { paymentInstance, user } from './baseUrl';
import JSONbig from 'json-bigint';
import { AxiosResponse } from 'axios';

const JSONbigString = JSONbig({ storeAsString: true });

export const signIn = async (payload: SignInForm) => {
  const res: AxiosResponse<UserInterface | DriverInterface> = await user.post('sign-in', payload, {
    transformResponse: (data) => JSONbigString.parse(data),
  });
  setAccessToken(res.data.token);
  return res.data;
};

export const signUp = async (payload: SignUpPayload) => {
  const res: AxiosResponse = await user.post('sign-up', payload);
  return res.data;
};

export const getUser = async () => {
  const res: AxiosResponse<UserInterface | DriverInterface> = await user.get('get-user', {
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    transformResponse: (data) => JSONbigString.parse(data),
  });
  return res.data;
};

export const updateInfoUser = async (payload: UpdateInfoUserPayload) => {
  const res: AxiosResponse = await user.put('update-info-user', payload, {
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
  });
  return res.data;
};

export const changePassword = async (payload: ChangePasswordPayload) => {
  const res: AxiosResponse = await user.put('change-password-user', payload, {
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
  });
  return res.data;
};

export const updateStatusDriver = async (payload: { status: number }) => {
  const res: AxiosResponse = await user.put('update-status-driver', payload, {
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
  });
  return res.data;
};

export const getTransportationCost = async (payload: GetTransportationCostPayload) => {
  const res: AxiosResponse<GetTransportationCostResponse> = await user.post(
    'get-transportation-cost',
    payload,
    {
      headers: {
        Authorization: `Bearer ${getAccessToken()}`,
      },
      transformResponse: (data) => JSONbigString.parse(data),
    }
  );
  return res.data;
};

export const getDriverEachWarehouse = async () => {
  const res: AxiosResponse<WarehouseDriver[]> = await user.get('get-driver-each-warehouse', {
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    transformResponse: (data) => JSONbigString.parse(data),
  });
  return res.data;
};

export const addAddressWarehouse = async (payload: AddAddressWarehousePayload) => {
  const res: AxiosResponse<WarehouseDriver[]> = await user.post('add-address-warehouse', payload, {
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
  });
  return res.data;
};

export const addStockKeeper = async (payload: AdminAddStockKeeperPayload) => {
  const res: AxiosResponse = await user.post('add-stocker', payload, {
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
  });
  return res.data;
};

export const addDriver = async (payload: AdminAddDriverPayload) => {
  const res: AxiosResponse = await user.post('add-driver', payload, {
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
  });
  return res.data;
};

export const getListDriver = async () => {
  const res: AxiosResponse<GetListDriverResponse[]> = await user.get('get-list-driver', {
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    transformResponse: (data) => JSONbigString.parse(data),
  });
  return res.data;
};

export const getUnusedVehicle = async () => {
  const res: AxiosResponse<Vehicle[]> = await user.get('get-vehicle-unused', {
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    transformResponse: (data) => JSONbigString.parse(data),
  });
  return res.data;
};

export const getListStocker = async () => {
  const res: AxiosResponse<GetListStockerResponse[]> = await user.get('get-list-stocker', {
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    transformResponse: (data) => JSONbigString.parse(data),
  });
  return res.data;
};

export const getListWarehouse = async () => {
  const res: AxiosResponse<WareHouse[]> = await user.get('get-list-warehouse', {
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    transformResponse: (data) => JSONbigString.parse(data),
  });
  return res.data;
};

export const payment = async (payload: PaymentPayload) => {
  const res: AxiosResponse<PaymentResponse> = await paymentInstance.post('create-order', payload, {
    headers: { Authorization: `Bearer ${getAccessToken()}` },
  });
  return res.data;
};
