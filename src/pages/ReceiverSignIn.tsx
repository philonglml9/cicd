import React, { useState } from 'react';
import styled from 'styled-components';
import { Form, Modal } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import { useLoading } from '../components/LoadingProvider';
import { alertError } from '../utils/alert';
import { EditButton, Label, PillInput } from '../components/CommonStyles';
import Particles from 'react-particles-js';
import bgImg from '../assets/login_bg.jpg';
import { getOrderInfoByReceiverPhoneNumber, submitOtp } from '../api/order';
import OtpInput from 'react-otp-input';
import { useOtpResult } from '../components/OtpResultProvider';

export const ReceiverSignIn = () => {
  const history = useHistory();
  const { setLoading } = useLoading();
  const { setResult } = useOtpResult();
  const [phone, setPhone] = useState('');
  const [orderId, setOrderId] = useState('');
  const [otp, setOtp] = useState('');
  const [isModalVisible, setIsModalVisible] = useState(false);

  const onFinish = async () => {
    if (orderId) {
      history.push(`/tracking?order_code=${orderId}`);
    } else {
      try {
        setLoading(true);
        const payload = {
          reqTime: Date.now(),
          receiverPhoneNumber: phone,
        };
        await getOrderInfoByReceiverPhoneNumber(payload);
        setIsModalVisible(true);
      } catch (err) {
        alertError(err.message);
      } finally {
        setLoading(false);
      }
    }
  };

  const onOtpChange = (value: string) => {
    setOtp(value);
  };

  const handleOk = async () => {
    try {
      setLoading(true);
      const payload = { otp: parseInt(otp), phoneNumber: phone, type: 1 };
      const { result } = await submitOtp(payload);
      setResult(result);
      history.push(`/receiver/order`);
    } catch (err) {
      alertError(err.message);
    } finally {
      setLoading(false);
    }
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    setOtp('');
  };

  const onPhoneChange = (e: any) => {
    setPhone(e.target.value);
  };

  const onOrderIdChange = (e: any) => {
    setOrderId(e.target.value);
  };

  return (
    <Wrapper>
      <BackgroundParticles
        params={{
          particles: {
            color: {
              value: ['#BD10E0', '#B8E986', '#50E3C2', '#FFD300', '#E86363'],
            },
            number: {
              value: 160,
              density: {
                enable: false,
              },
            },
            size: {
              value: 3,
              random: true,
              anim: {
                speed: 4,
                size_min: 0.3,
              },
            },
            line_linked: {
              enable: false,
            },
            move: {
              random: true,
              speed: 1,
              direction: 'top',
              out_mode: 'out',
            },
          },
          interactivity: {
            modes: {
              bubble: {
                distance: 250,
                duration: 2,
                size: 0,
                opacity: 0,
              },
              repulse: {
                distance: 400,
                duration: 4,
              },
            },
          },
        }}
      />

      <ImageWrapper>
        <SideImage src={bgImg} alt="bgImage" />
      </ImageWrapper>

      <Container>
        <FormWrapper>
          <Form name="normal_login" onFinish={onFinish}>
            <Label>Đăng nhập bằng số điện thoại</Label>
            <Form.Item name="phone">
              <PillInput
                disabled={!!orderId}
                onChange={onPhoneChange}
                size="large"
                prefix={<UserOutlined />}
                placeholder="Nhập số điện thoại"
              />
            </Form.Item>

            <Label>Hoặc nhập mã yêu cầu để tra cứu</Label>
            <Form.Item name="orderId">
              <PillInput
                disabled={!!phone}
                onChange={onOrderIdChange}
                size="large"
                prefix={<UserOutlined />}
                placeholder="Nhập mã yêu cầu"
              />
            </Form.Item>

            <Form.Item>
              <EditButton size="large" type="primary" htmlType="submit">
                Xác nhận
              </EditButton>
            </Form.Item>
          </Form>

          <ToRegister>
            Về lại trang <Link to="/signin">đăng nhập cho người gửi</Link>
          </ToRegister>
        </FormWrapper>
      </Container>

      <Modal
        title="Nhập mã OTP gửi đến địa chỉ mail"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        okText="Xác nhận"
      >
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <OtpInput
            value={otp}
            numInputs={6}
            separator={<span style={{ margin: '10px' }}>-</span>}
            onChange={onOtpChange}
            inputStyle={{ width: '30px', border: '1px solid black' }}
          />
        </div>
      </Modal>
    </Wrapper>
  );
};

const Container = styled.div`
  width: 60%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const FormWrapper = styled.div`
  width: 500px;
`;

const ToRegister = styled.h3`
  text-align: center;
`;

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  height: 100%;
`;

const ImageWrapper = styled.div`
  background-image: linear-gradient(180deg, #aaaafe 12%, #6653ff 90%);
  width: 40%;
`;

const SideImage = styled.img`
  height: 100%;
  width: 100%;
`;

const BackgroundParticles = styled(Particles)`
  position: fixed !important;
  right: 0;
  top: 0;
  width: 60%;
  height: 100%;
  z-index: -1;
`;
