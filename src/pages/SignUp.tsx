import React, { useState } from 'react';
import styled from 'styled-components';
import { Form } from 'antd';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import { signUp } from '../api/user';
import { SignUpPayload } from '../interface/Auth';
import { useLoading } from '../components/LoadingProvider';
import { alertError } from '../utils/alert';
import { FormInstance } from 'antd/lib/form';
import { EditButton, Label, PillInput } from '../components/CommonStyles';
import GooglePlacesAutocomplete from 'react-google-places-autocomplete';
import { getGoogleApiKey } from '../utils/getGoogleKey';
import { states } from '../constants/states';
import bgImg from '../assets/login_bg.jpg';
import Particles from 'react-particles-js';
import ReactGA from 'react-ga';

export const SignUp = () => {
  const history = useHistory();
  const [form]: FormInstance<SignUpPayload>[] = Form.useForm();
  const { setLoading } = useLoading();
  const [, forceUpdate] = useState([]);

  const onFinish = async () => {
    const { password, rePassword } = form.getFieldsValue();
    if (password !== rePassword) {
      alertError('Nhập lại mật khẩu không khớp!');
      return;
    }

    try {
      setLoading(true);
      await signUp({ ...form.getFieldsValue() });
      ReactGA.event({
        category: 'User',
        action: 'User sign up',
      });
      history.push('/signin');
    } catch (err) {
      alertError(err.message);
    } finally {
      setLoading(false);
    }
  };

  const onPlaceSearch = (value: any) => {
    const stateName = value.value.terms[value.value.terms.length - 2].value;
    const findProvinceCode = states.find((state) => state.label === stateName)?.value || 0;

    form.setFieldsValue({
      ...form.getFieldsValue(),
      provinceCode: findProvinceCode,
      address: value.label,
    });

    forceUpdate([]);
  };

  return (
    <Wrapper>
      <BackgroundParticles
        params={{
          particles: {
            color: {
              value: ['#BD10E0', '#B8E986', '#50E3C2', '#FFD300', '#E86363'],
            },
            number: {
              value: 160,
              density: {
                enable: false,
              },
            },
            size: {
              value: 3,
              random: true,
              anim: {
                speed: 4,
                size_min: 0.3,
              },
            },
            line_linked: {
              enable: false,
            },
            move: {
              random: true,
              speed: 1,
              direction: 'top',
              out_mode: 'out',
            },
          },
          interactivity: {
            modes: {
              bubble: {
                distance: 250,
                duration: 2,
                size: 0,
                opacity: 0,
              },
              repulse: {
                distance: 400,
                duration: 4,
              },
            },
          },
        }}
      />

      <ImageWrapper>
        <SideImage src={bgImg} alt="bgImage" />
      </ImageWrapper>

      <Container>
        <FormWrapper>
          <Title>Tạo tài khoản</Title>
          <Form name="normal_login" onFinish={onFinish} form={form}>
            <Label>Email</Label>
            <Form.Item name="email" rules={[{ required: true, message: 'Vui lòng nhập email' }]}>
              <PillInput size="large" type="email" placeholder="Nhập email" />
            </Form.Item>

            <Label>Số điện thoại</Label>
            <Form.Item
              name="phone"
              rules={[{ required: true, message: 'Vui lòng nhập số điện thoại' }]}
            >
              <PillInput size="large" type="tel" placeholder="Nhập số điện thoại" />
            </Form.Item>

            <Label>Tên tài khoản</Label>
            <Form.Item
              name="username"
              rules={[{ required: true, message: 'Vui lòng nhập tên tài khoản' }]}
            >
              <PillInput size="large" type="text" placeholder="Nhập tên tài khoản" />
            </Form.Item>

            <Label>Địa chỉ</Label>
            <Form.Item
              name="address"
              rules={[{ required: true, message: 'Vui lòng nhập Địa chỉ' }]}
            >
              <GooglePlacesAutocomplete
                apiOptions={{ language: 'vi', region: 'vi' }}
                apiKey={getGoogleApiKey()}
                selectProps={{
                  value: {
                    label: form.getFieldValue('address') || '',
                    value: form.getFieldValue('address') || '',
                  },
                  onChange: onPlaceSearch,
                  styles: {
                    control: (provided: any) => ({
                      ...provided,
                      borderRadius: '20px',
                    }),
                  },
                }}
              />
            </Form.Item>

            <Form.Item style={{ display: 'none' }} name="provinceCode">
              <PillInput />
            </Form.Item>

            <Label>Mật khẩu</Label>
            <Form.Item
              name="password"
              rules={[{ required: true, message: 'Vui lòng nhập mật khẩu của bạn' }]}
            >
              <PillInput size="large" type="password" placeholder="Nhập mật khẩu" />
            </Form.Item>

            <Label>Nhập lại mật khẩu</Label>
            <Form.Item
              name="rePassword"
              rules={[
                {
                  required: true,
                  message: 'Vui lòng xác nhận lại mật khẩu của bạn',
                },
              ]}
            >
              <PillInput size="large" type="password" placeholder="Nhập lại mật khẩu" />
            </Form.Item>

            <Form.Item>
              <EditButton size="large" type="primary" htmlType="submit">
                Đăng ký
              </EditButton>
            </Form.Item>
          </Form>

          <ToRegister>
            Bạn đã có tài khoản? <Link to="/signin">Đăng nhập ngay</Link>
          </ToRegister>
        </FormWrapper>
      </Container>
    </Wrapper>
  );
};

const Container = styled.div`
  width: 60%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const FormWrapper = styled.div`
  width: 500px;
`;

const Title = styled.div`
  font-size: 24px;
  text-align: center;
`;

const ToRegister = styled.h3`
  text-align: center;
`;

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  height: 100%;
`;

const ImageWrapper = styled.div`
  background-image: linear-gradient(180deg, #aaaafe 12%, #6653ff 90%);
  width: 40%;
`;

const SideImage = styled.img`
  height: 100%;
  width: 100%;
`;

const BackgroundParticles = styled(Particles)`
  position: fixed !important;
  right: 0;
  top: 0;
  width: 60%;
  height: 100%;
  z-index: -1;
`;
