import { useState, useEffect } from 'react';
import { getUnusedVehicle } from '../api/user';
import { useLoading } from '../components/LoadingProvider';
import { Vehicle } from '../interface/Auth';
import { alertError } from '../utils/alert';

export const useUnusedVehicles = () => {
  const { setLoading } = useLoading();
  const [unusedVehicles, setUnusedVehicles] = useState<Vehicle[]>([]);

  useEffect(() => {
    const callGetUnusedVehicle = async () => {
      try {
        setLoading(true);
        const res = await getUnusedVehicle();
        const vehiclesWithTableKey = res.map((vehicle) => ({
          ...vehicle,
          key: vehicle.id,
        }));
        setUnusedVehicles(vehiclesWithTableKey);
      } catch (err) {
        alertError(err.message);
      } finally {
        setLoading(false);
      }
    };

    callGetUnusedVehicle();
  }, [setLoading]);

  return { unusedVehicles };
};
