import { useState, useEffect } from 'react';
import { getListWarehouse } from '../api/user';
import { useLoading } from '../components/LoadingProvider';
import { WareHouse } from '../interface/Auth';
import { alertError } from '../utils/alert';

export const useWarehouses = () => {
  const { setLoading } = useLoading();
  const [warehouses, setWarehouses] = useState<WareHouse[]>([]);

  useEffect(() => {
    const callGetUnusedVehicle = async () => {
      try {
        setLoading(true);
        const res = await getListWarehouse();
        const wareHousesWithTableKey = res.map((vehicle) => ({
          ...vehicle,
          key: vehicle.id,
        }));
        setWarehouses(wareHousesWithTableKey);
      } catch (err) {
        alertError(err.message);
      } finally {
        setLoading(false);
      }
    };

    callGetUnusedVehicle();
  }, [setLoading]);

  return { warehouses };
};
