import { useEffect, useState } from 'react';
import { getListDriver } from '../api/user';
import { useLoading } from '../components/LoadingProvider';
import { GetListDriverResponse } from '../interface/Auth';
import { alertError } from '../utils/alert';

export const useGetListDriver = () => {
  const [drivers, setDrivers] = useState<GetListDriverResponse[]>([]);

  const { setLoading } = useLoading();
  useEffect(() => {
    const callGetListDriver = async () => {
      try {
        setLoading(true);
        const res = await getListDriver();
        const driversWithTableKey = res.map((driver) => ({
          ...driver,
          key: driver.id,
        }));
        setDrivers(driversWithTableKey);
      } catch (err) {
        alertError(err.message);
      } finally {
        setLoading(false);
      }
    };

    callGetListDriver();
  }, [setLoading]);

  return { drivers };
};
