/* eslint-disable jsx-a11y/anchor-is-valid */
import { PDFDownloadLink } from '@react-pdf/renderer';
import { Modal, Table, Tabs } from 'antd';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import styled from 'styled-components';
import { DownloadOutlined } from '@ant-design/icons';
import { getExportInfoByStockRequest, getImportInfoByStockRequest } from '../../api/order';
import { BlueText, InfoContainer, InfoTitle, PillButton } from '../../components/CommonStyles';
import { useLoading } from '../../components/LoadingProvider';
import { useUser } from '../../components/UserProvider';
// import { mockImportExportTicket } from '../../constants/mockTableData';
import { StockKeeperInterface } from '../../interface/Auth';
import { DetailOrder, ImportExportEntity } from '../../interface/Order';
import { getAccessToken } from '../../utils/accessToken';
import { alertError } from '../../utils/alert';
import { ImportExportOrdersPDF } from './ImportExportOrdersPDF';
import { ImportExport } from './StockMixin';
import { EmptyTableContent } from '../../components/EmptyTable';
import {
  UserOutlined,
  PhoneOutlined,
  CalendarOutlined,
  FieldNumberOutlined,
} from '@ant-design/icons';

export const StockHistory = () => {
  const history = useHistory();
  const { user } = useUser();
  const { setLoading } = useLoading();

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [exports, setExports] = useState<ImportExportEntity[]>([]);
  const [imports, setImports] = useState<ImportExportEntity[]>([]);
  const [selectedTicket, setSelectedTicket] = useState(null);

  useEffect(() => {
    const callGetExportInfoByStockRequest = async () => {
      const payload = {
        stockId: (user as StockKeeperInterface).warehouseResponse.id,
        accessToken: getAccessToken(),
        reqTime: Date.now(),
      };

      try {
        setLoading(true);
        const { importExportResponses } = await getExportInfoByStockRequest(payload);

        const temp = importExportResponses.map((item) => ({
          ...item,
          key: item.id,
        }));

        setExports(temp);
      } catch (err) {
        alertError(err.message);
      } finally {
        setLoading(false);
      }
    };
    if (user) callGetExportInfoByStockRequest();
  }, [setLoading, user]);

  useEffect(() => {
    const callGetImportInfoByStockRequest = async () => {
      const payload = {
        stockId: (user as StockKeeperInterface).warehouseResponse.id,
        accessToken: getAccessToken(),
        reqTime: Date.now(),
      };

      try {
        setLoading(true);
        const { importExportResponses } = await getImportInfoByStockRequest(payload);

        const importExportResponsesWithKey = importExportResponses.map((item) => ({
          ...item,
          key: item.id,
        }));

        // const { data } = mockImportExportTicket;
        // const { importExportResponses } = data;

        setImports(importExportResponsesWithKey);
      } catch (err) {
        alertError(err.message);
      } finally {
        setLoading(false);
      }
    };
    if (user) callGetImportInfoByStockRequest();
  }, [setLoading, user]);

  const onTabChange = (key: any) => {
    history.push(key);
  };

  const onDetailClick = (order: ImportExportEntity) => {
    setIsModalVisible(true);
    const flattenSubOrderResponses = order.subOrderResponses.map((subOrder) => {
      const detailOrder: DetailOrder = JSON.parse(subOrder.detailOrder);
      return { ...subOrder, ...detailOrder };
    });

    setSelectedTicket({
      ...order,
      subOrderResponses: flattenSubOrderResponses,
    });
  };

  const onOk = () => {
    setIsModalVisible(false);
  };

  const columns = [
    {
      title: 'ID',
      render: (_: any, order: ImportExportEntity) => {
        const { id, time } = order;
        return (
          <BlueText>
            <p>Mã phiếu: {id}</p>
            <p>
              <CalendarOutlined /> {new Date(time).toLocaleString()}
            </p>
          </BlueText>
        );
      },
    },
    {
      title: 'Thủ kho',
      render: (_: any, order: ImportExportEntity) => {
        const { stockkeeperId, stockKeeperName, stockKeeperPhone } = order;

        return (
          <BlueText>
            <p>
              <FieldNumberOutlined /> <strong>{stockkeeperId}</strong>
            </p>
            <p>
              <UserOutlined /> <strong>{stockKeeperName}</strong>
            </p>
            <p>
              <PhoneOutlined /> <strong>{stockKeeperPhone}</strong>
            </p>
          </BlueText>
        );
      },
    },
    {
      title: 'Tài xế',
      render: (_: any, order: ImportExportEntity) => {
        const { driverId, driverName, driverPhone } = order;

        return driverId ? (
          <BlueText>
            <p>
              <FieldNumberOutlined /> <strong>{driverId}</strong>
            </p>
            <p>
              <UserOutlined /> <strong>{driverName}</strong>
            </p>
            <p>
              <PhoneOutlined /> <strong>{driverPhone}</strong>
            </p>
          </BlueText>
        ) : (
          <BlueText>Gửi/nhận trực tiếp tại kho</BlueText>
        );
      },
    },
    {
      title: 'Các đơn hàng',
      render: (_: any, order: ImportExportEntity) => {
        return (
          <BlueText>
            <p>
              <a onClick={() => onDetailClick(order)}>Click để xem chi tiết</a>
            </p>
          </BlueText>
        );
      },
    },
  ];

  const renderModalFooter = () => {
    const type = history.location.hash === '#2' ? ImportExport.Export : ImportExport.Import;

    return (
      <PillButton type="primary">
        <PDFDownloadLink
          document={
            selectedTicket && (
              <ImportExportOrdersPDF
                type={type}
                driverId={selectedTicket.driverId}
                stockKeeper={user as StockKeeperInterface}
                orders={selectedTicket.subOrderResponses}
                time={selectedTicket.time}
              />
            )
          }
          fileName="receipt.pdf"
        >
          {({ loading }) =>
            loading ? (
              'Loading document...'
            ) : (
              <span>
                <DownloadOutlined /> Tải phiếu nhập/xuất
              </span>
            )
          }
        </PDFDownloadLink>
      </PillButton>
    );
  };

  return (
    <OrdersContainer>
      <Tabs defaultActiveKey={history.location.hash} centered onChange={onTabChange}>
        <TabPane tab="Lịch sử nhập" key="#1">
          <Table
            locale={{ emptyText: <EmptyTableContent /> }}
            bordered
            columns={columns}
            dataSource={imports}
          />
        </TabPane>
        <TabPane tab="Lịch sử xuất" key="#2">
          <Table
            locale={{ emptyText: <EmptyTableContent /> }}
            bordered
            columns={columns}
            dataSource={exports}
          />
        </TabPane>
      </Tabs>

      <Modal
        title="Chi tiết các đơn hàng"
        width={700}
        visible={isModalVisible}
        onCancel={() => setIsModalVisible(false)}
        onOk={onOk}
        okText="Tải file PDF"
        footer={renderModalFooter()}
      >
        {selectedTicket?.subOrderResponses.map((subOrder: any) => {
          return (
            <div key={subOrder.subOrderId}>
              <InfoTitle>| {subOrder.subOrderId}</InfoTitle>
              <InfoContainer>
                <p>
                  Loại hàng: <strong>{subOrder.kind}</strong>
                </p>
                <p>
                  Khối lượng: <strong>{subOrder.weight}</strong>
                </p>
                <p>
                  Người gửi:{' '}
                  <strong>
                    {subOrder.nameSender}, SĐT: {subOrder.phoneSender}
                  </strong>
                </p>
                <p>
                  Người nhận:{' '}
                  <strong>
                    {subOrder.nameReceiver}, SĐT: {subOrder.phoneReceiver}
                  </strong>
                </p>
                <p>
                  Địa chỉ gửi: <strong>{subOrder.fromAddress}</strong>
                </p>
                <p>
                  Địa chỉ đến: <strong>{subOrder.toAddress}</strong>
                </p>
              </InfoContainer>
            </div>
          );
        })}
      </Modal>
    </OrdersContainer>
  );
};

const { TabPane } = Tabs;

const OrdersContainer = styled.div`
  margin: auto;
  max-width: 90%;
  margin-bottom: 100px;
  margin-top: 50px;
`;
