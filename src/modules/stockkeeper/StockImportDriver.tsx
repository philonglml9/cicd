import React, { useState } from 'react';
import { Table, Space, Modal } from 'antd';
import { FlexCenter, PillButton, PillSelect } from '../../components/CommonStyles';
import { useLoading } from '../../components/LoadingProvider';
import { alertError } from '../../utils/alert';
import { getAccessToken } from '../../utils/accessToken';
import { importSubOrders } from '../../api/order';
import { SuborderStatus } from '../../interface/Order';
import { useUser } from '../../components/UserProvider';
import { GetListDriverResponse, StockKeeperInterface } from '../../interface/Auth';
import { PDFDownloadLink } from '@react-pdf/renderer';
import { ImportExportOrdersPDF } from './ImportExportOrdersPDF';
import {
  Container,
  getOrdersTableDataByDriver,
  ImportExport,
  requestColumns,
  FlattenSubOrderByDriver,
} from './StockMixin';
import { useGetListDriver } from '../../hooks/useGetListDriver';
import { EmptyTableContent } from '../../components/EmptyTable';
import ReactGA from 'react-ga';

export const StockImportDriver = () => {
  const { user } = useUser();
  const { drivers } = useGetListDriver();
  const { setLoading } = useLoading();
  const [selectedDriver, setSelectedDriver] = useState<GetListDriverResponse>(null);
  const [orders, setOrders] = useState<FlattenSubOrderByDriver[]>(null);
  let importTime: number;

  const onImportOrders = async () => {
    const subOrderIds = orders.map((order) => order.subOrderId);
    importTime = Date.now();
    const payload = {
      accessToken: getAccessToken(),
      reqTime: importTime,

      subOrderIds,
      stockKeeperId: user.id,
      stockId: (user as StockKeeperInterface).warehouseResponse.id,
      driverId: selectedDriver.id,

      stockKeeperName: (user as StockKeeperInterface).username,
      driverName: selectedDriver.username,
      stockKeeperPhone: (user as StockKeeperInterface).phone,
      driverPhone: selectedDriver.phone || '09180321242',
    };
    try {
      await importSubOrders(payload);
      ReactGA.event({
        category: 'Stockkeeper',
        action: 'Stockkeeper import driver',
      });
      Modal.success({
        title: 'Nhập đơn hàng thành công',
        content: (
          <PDFDownloadLink
            document={
              <ImportExportOrdersPDF
                type={ImportExport.Import}
                driver={selectedDriver}
                stockKeeper={user as StockKeeperInterface}
                orders={orders}
                time={importTime}
              />
            }
            fileName="import.pdf"
          >
            Tải phiếu nhập kho
          </PDFDownloadLink>
        ),
      });

      setOrders(null);
      setSelectedDriver(null);
    } catch (err) {
      alertError(err.message);
    }
  };

  const onClick = () => {
    Modal.confirm({
      title: 'Nhập đơn hàng trong xe vào kho?',
      onOk: onImportOrders,
    });
  };

  const findOrder = async (value: string) => {
    setSelectedDriver(drivers.find((driver) => driver.id === value));

    try {
      setLoading(true);
      const ordersTableData = await getOrdersTableDataByDriver(value);
      const filtedOrdersTableData = ordersTableData.filter(
        (subOrder) =>
          subOrder.orderStatus === SuborderStatus.COMING_SEND_STOCK ||
          (subOrder.orderStatus === SuborderStatus.TRANSFERING_ARRIVED_STOCK &&
            (subOrder.srcStockId === (user as StockKeeperInterface).warehouseResponse.id ||
              subOrder.destStockId === (user as StockKeeperInterface).warehouseResponse.id))
      );

      setOrders(filtedOrdersTableData);
    } catch (err) {
      alertError(err.message);
    } finally {
      setLoading(false);
    }
  };

  return (
    <Container>
      <FlexCenter>
        <Space>
          <span>
            <strong>Nhập ID tài xế: </strong>
          </span>
          <PillSelect
            showSearch
            style={{ width: 200 }}
            value={selectedDriver?.id}
            onChange={findOrder}
            filterOption={true}
          >
            {drivers.map((driver) => {
              return (
                <Option key={driver.id} value={driver.id}>
                  {driver.id}
                </Option>
              );
            })}
          </PillSelect>
        </Space>
      </FlexCenter>
      <br />
      <PillButton onClick={onClick} disabled={!selectedDriver} type="primary">
        Nhập đơn hàng trong xe vào kho
      </PillButton>
      <br />
      <br />
      <Table
        bordered
        columns={requestColumns}
        dataSource={orders}
        pagination={{ pageSize: 3, hideOnSinglePage: true }}
        locale={{ emptyText: <EmptyTableContent /> }}
      />
    </Container>
  );
};

const { Option } = PillSelect;
