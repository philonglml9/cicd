import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Form, Modal } from 'antd';
import { useUser } from '../../components/UserProvider';
import { FormInstance } from 'antd/lib/form';
import { EditButton, Label, PillInput } from '../../components/CommonStyles';
import { StockKeeperInterface } from '../../interface/Auth';

export const StockAccount = () => {
  const { user } = useUser();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [form]: FormInstance<any>[] = Form.useForm();

  useEffect(() => {
    form.setFieldsValue({
      ...user,
      warehouseId: (user as StockKeeperInterface)?.warehouseResponse.id,
      warehouseAddress: (user as StockKeeperInterface)?.warehouseResponse.address,
    });
  }, [form, user]);

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const onFinish = () => {
    // const foo = form.getFieldsValue();
    // console.log('Received values of form: ', foo);
  };

  const onChangePassClick = () => {
    setIsModalVisible(true);
  };

  return (
    <Container>
      <Title>Chỉnh sửa thông tin tài khoản</Title>
      <Form
        name="normal_login"
        onFinish={onFinish}
        form={form}
        initialValues={{ ...user, password: 'password' }}
      >
        <Label>ID thủ kho</Label>
        <Form.Item name="id">
          <PillInput disabled size="large" />
        </Form.Item>

        <Label>Tên</Label>
        <Form.Item name="username" rules={[{ required: true }]}>
          <PillInput size="large" />
        </Form.Item>

        <Label>Email</Label>
        <Form.Item name="email" rules={[{ required: true, message: 'Vui lòng nhập email' }]}>
          <PillInput disabled size="large" type="email" />
        </Form.Item>

        <Label>Số điện thoại</Label>
        <Form.Item
          name="phone"
          rules={[{ required: true, message: 'Vui lòng nhập số điện thoại' }]}
        >
          <PillInput disabled size="large" type="tel" />
        </Form.Item>

        <Label>ID kho</Label>
        <Form.Item name="warehouseId">
          <PillInput disabled size="large" />
        </Form.Item>

        <Label>Địa chỉ kho</Label>
        <Form.Item name="warehouseAddress">
          <PillInput disabled size="large" />
        </Form.Item>

        <Label>Mật khẩu</Label>
        <Form.Item name="password">
          <PillInput disabled size="large" type="password" />
        </Form.Item>

        <ChangePassword onClick={onChangePassClick}>Đổi mật khẩu</ChangePassword>

        <Form.Item>
          <EditButton htmlType="submit" size="large" type="primary">
            Cập nhật
          </EditButton>
        </Form.Item>
      </Form>

      <Modal
        title="Đổi mật khẩu đăng nhập"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        okText="Hoàn thành"
      >
        <Form name="change_password">
          <Label>Mật khẩu cũ</Label>
          <Form.Item name="password">
            <PillInput size="large" type="password" />
          </Form.Item>

          <Label>Mật khẩu mới</Label>
          <Form.Item name="newPassword">
            <PillInput size="large" type="password" />
          </Form.Item>

          <Label>Nhập lại mật khẩu mới</Label>
          <Form.Item name="reNewPassword">
            <PillInput size="large" type="password" />
          </Form.Item>
        </Form>
      </Modal>
    </Container>
  );
};

const Container = styled.div`
  width: 500px;
  margin: auto;
  margin-top: 2rem;
`;

const Title = styled.h1`
  font-weight: bold;
  text-align: center;
`;

const ChangePassword = styled.span`
  cursor: pointer;
  color: #00467f;
  font-weight: bold;
`;
