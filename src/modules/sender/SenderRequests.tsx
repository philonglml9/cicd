import React, { ChangeEventHandler, useEffect, useState } from 'react';
import { Table, Select, Modal } from 'antd';
import { cancelOrder, getOrderByUser } from '../../api/order';
import { getAccessToken } from '../../utils/accessToken';
import { useLoading } from '../../components/LoadingProvider';
import { useUser } from '../../components/UserProvider';
import { alertError, alertSuccess } from '../../utils/alert';
import {
  DetailOrder,
  OrderByUser,
  OrderStatus,
  OrderStatusText,
  PaymentStatus,
} from '../../interface/Order';
import {
  MailOutlined,
  UserOutlined,
  PhoneOutlined,
  HomeOutlined,
  CalendarOutlined,
  FieldNumberOutlined,
  CodeSandboxOutlined,
  DollarOutlined,
} from '@ant-design/icons';
import {
  BlueText,
  FilterWrapper,
  FlexCenter,
  Label,
  PillButton,
  PillDatePicker,
  PillInput,
  PillSelect,
} from '../../components/CommonStyles';
// import { mockSenderRequestData } from '../../constants/mockTableData';
import { endOfDay, startOfDay } from 'date-fns';
import { EmptyTableContent } from '../../components/EmptyTable';
import { ReactComponent as WeightIcon } from '../../assets/weight.svg';
import TextArea from 'antd/lib/input/TextArea';
import { isDevelopment } from '../../utils/isDevelopment';

interface TableData extends OrderByUser, DetailOrder {
  key: string;
  status: string;
}

export const SenderRequests = () => {
  const { setLoading } = useLoading();
  const { user } = useUser();

  const [orderFilter, setOrderFilter] = useState('');
  const [status, setStatus] = useState('None');
  const [fromDay, setFromDay] = useState(0);
  const [toDay, setToDay] = useState(Infinity);
  const [requests, setRequests] = useState<TableData[]>([]);

  const [selectedRequest, setSelectedRequest] = useState<TableData>(null);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [reason, setReason] = useState('');

  useEffect(() => {
    const callGetOrderByUser = async () => {
      const payload = {
        userId: user.id,
        accessToken: getAccessToken(),
        reqTime: Date.now(),
      };

      try {
        // console.log('order', isLoading, Date.now());
        setLoading(true);

        const { orders } = await getOrderByUser(payload);

        const ordersTableData = orders?.map((order) => {
          const detailOrder: DetailOrder = JSON.parse(order.detailOrder);
          return {
            key: order.orderId,
            ...order,
            ...detailOrder,
            status: OrderStatusText[order.orderStatus],
          };
        });
        setRequests(ordersTableData);

        // setRequests(mockSenderRequestData);
      } catch (err) {
        alertError(err.message);
      } finally {
        setLoading(false);
      }
    };

    if (user) callGetOrderByUser();
  }, [setLoading, user]);

  const filterRequestData = requests
    .filter((data) => {
      return data.orderStatus !== OrderStatus.CANCELED;
    })
    .filter((data) => {
      if (orderFilter === '') return true;
      return `${data.orderId}`.toLowerCase().includes(orderFilter.toLowerCase());
    })
    .filter((data) => {
      if (status === 'None') return true;
      if (status === OrderStatusText[3]) {
        return data.orderStatus === 3 && data.paymentStatus === 1;
      }
      if (status === OrderStatusText[4]) {
        return data.orderStatus === 3 && data.paymentStatus === 2;
      }
      return data.status === status;
    })
    .filter((data) => {
      return data.orderTime >= fromDay;
    })
    .filter((data) => {
      return data.orderTime <= toDay;
    });

  const onDeleteClick = (request: TableData) => {
    setIsModalVisible(true);
    setSelectedRequest(request);
  };

  const onDeleteOk = async () => {
    const payload = {
      reqTime: Date.now(),
      accessToken: getAccessToken(),
      orderId: selectedRequest?.orderId,
      reason,
    };

    try {
      setLoading(true);
      await cancelOrder(payload);
      alertSuccess('Xóa yêu cầu thành công');
      setSelectedRequest(null);
      setIsModalVisible(false);
      setRequests(requests.filter((item) => item.orderId !== selectedRequest?.orderId));
    } catch (err) {
      alertError(err.message);
    } finally {
      setLoading(false);
    }
  };

  const onTextAreaChange = (e: any) => {
    setReason(e.target.value);
  };

  const requestColumns = [
    {
      title: 'Mã yêu cầu',
      render: (_: any, request: TableData) => {
        const { orderId, orderTime, remainWeight, totalWeight } = request;
        return (
          <BlueText>
            <p>
              <FieldNumberOutlined /> {orderId}
            </p>
            <p>
              <CalendarOutlined /> {new Date(orderTime).toLocaleString()}
            </p>
            {request.orderStatus === OrderStatus.PROCESSING && remainWeight === totalWeight && (
              <PillButton onClick={() => onDeleteClick(request)} danger type="primary">
                Hủy yêu cầu
              </PillButton>
            )}
          </BlueText>
        );
      },
    },
    {
      title: 'Bên nhận',
      render: (_: any, request: TableData) => {
        const { nameReceiver, toAddress, phoneReceiver, emailReceiver } = request;
        return (
          <BlueText>
            <p>
              <UserOutlined /> {nameReceiver}
            </p>
            <p>
              <PhoneOutlined /> {phoneReceiver}
            </p>
            <p>
              <MailOutlined /> {emailReceiver}
            </p>
            <p>
              <HomeOutlined /> {toAddress}
            </p>
          </BlueText>
        );
      },
    },
    {
      title: 'Hàng hóa',
      render: (_: any, request: TableData) => {
        const { fee, totalWeight, kind } = request;

        return (
          <BlueText>
            <p>
              <CodeSandboxOutlined /> {kind}
            </p>
            <p>
              <WeightIcon /> {totalWeight.toLocaleString()} kg
            </p>

            <p style={{ color: 'red' }}>
              <DollarOutlined />{' '}
              {fee.toLocaleString('it-IT', {
                style: 'currency',
                currency: 'VND',
              })}
            </p>
          </BlueText>
        );
      },
    },
    {
      title: 'Thông tin thêm',
      render: (_: any, request: TableData) => {
        const {
          description,
          value,
          paymentSide,
          directlyReceive,
          directlySend,
          addressDestWarehouse,
          addressSrcWarehouse,
          status,
          remainWeight,
          paymentStatus,
          orderStatus,
        } = request;
        const renderStatus = () => {
          if (orderStatus === OrderStatus.DONE) {
            if (paymentStatus === PaymentStatus.WAITING) return OrderStatusText[3];
            else return OrderStatusText[4];
          }
          return status;
        };
        return (
          <BlueText>
            <p style={{ color: 'red' }}>Trạng thái: {renderStatus()}</p>
            <p>
              Giá trị hàng hóa: <span style={{ color: 'red' }}>{value.toLocaleString()} VND</span>
            </p>
            <p>Ghi chú: {description}</p>
            {paymentSide === 'sender' ? <p>Bên gửi trả phí</p> : <p>Bên nhận trả phí</p>}
            {remainWeight ? <p>Khối lượng chưa lấy: {remainWeight.toLocaleString()}kg</p> : null}
            <p>{directlySend && `Gửi hàng trực tiếp tại kho: ${addressSrcWarehouse}`}</p>
            <p>{directlyReceive && `Nhận hàng trực tiếp tại kho: ${addressDestWarehouse}`}</p>
          </BlueText>
        );
      },
    },
    isDevelopment()
      ? {
          title: 'assignedDrivers',
          render: (_: any, request: TableData) => {
            const { assignedDrivers } = request;
            const assignedDriverList = assignedDrivers.split(',');
            return (
              <>
                {assignedDriverList.map((item) => (
                  <p key={item}>{item}</p>
                ))}
              </>
            );
          },
        }
      : {},
    isDevelopment()
      ? {
          title: 'stockId',
          render: (_: any, request: TableData) => {
            const { srcStockId, destStockId } = request;
            return (
              <>
                <p>srcStockId: {srcStockId}</p>
                <p>destStockId: {destStockId}</p>
              </>
            );
          },
        }
      : {},
  ];

  const onOrderFilterChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    setOrderFilter(e.target.value);
  };

  const filterStatus = (value: any) => {
    setStatus(value);
  };

  const onFromDayChange = (date: any) => {
    setFromDay(date ? startOfDay(date.valueOf()).getTime() : 0);
  };

  const onToDayChange = (date: any) => {
    setToDay(date ? endOfDay(date.valueOf()).getTime() : Infinity);
  };

  return (
    <>
      <FlexCenter>
        <FilterWrapper>
          <Label>Mã yêu cầu</Label>
          <PillInput
            onChange={onOrderFilterChange}
            placeholder="Lọc theo mã yêu cầu"
            value={orderFilter}
          />
        </FilterWrapper>

        <FilterWrapper>
          <Label>Trạng thái</Label>
          <PillSelect defaultValue="None" onChange={filterStatus}>
            <Option value="None">None</Option>
            <Option value={OrderStatusText[1]}>{OrderStatusText[1]}</Option>
            <Option value={OrderStatusText[2]}>{OrderStatusText[2]}</Option>
            <Option value={OrderStatusText[3]}>{OrderStatusText[3]}</Option>
            <Option value={OrderStatusText[4]}>{OrderStatusText[4]}</Option>
          </PillSelect>
        </FilterWrapper>

        <FilterWrapper>
          <Label>Từ đầu ngày</Label>
          <PillDatePicker onChange={onFromDayChange} />
        </FilterWrapper>

        <FilterWrapper>
          <Label>Đến cuối ngày</Label>
          <PillDatePicker onChange={onToDayChange} />
        </FilterWrapper>

        <FilterWrapper
          style={{
            display: 'flex',
            alignItems: 'center',
          }}
        >
          <BlueText>
            Hiển thị <span style={{ color: '#f26522' }}>{filterRequestData.length}</span> yêu cầu
          </BlueText>
        </FilterWrapper>
      </FlexCenter>
      <br />
      <Table
        bordered
        columns={requestColumns}
        dataSource={filterRequestData}
        pagination={{ pageSize: 3, hideOnSinglePage: true }}
        locale={{ emptyText: <EmptyTableContent /> }}
      />

      <Modal
        title="Bạn có chắc muốn hủy yêu cầu?"
        visible={isModalVisible}
        onOk={onDeleteOk}
        okButtonProps={{ danger: true }}
        onCancel={() => setIsModalVisible(false)}
        okText="Hủy yêu cầu"
      >
        <TextArea
          value={reason}
          onChange={onTextAreaChange}
          rows={5}
          placeholder="Ghi lí do bạn muốn hủy yêu cầu"
        />
      </Modal>
    </>
  );
};

const { Option } = Select;
