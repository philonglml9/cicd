import React from 'react';
import { SenderForm } from './SenderForm';

export const SenderEdit = () => {
  return <SenderForm isEditMode />;
};
