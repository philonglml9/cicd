import { Table } from 'antd';
import { endOfDay, startOfDay } from 'date-fns';
import React, { ChangeEventHandler, useEffect, useState } from 'react';
import { getSubOrderByUser } from '../../api/order';
import {
  BlueText,
  FilterWrapper,
  FlexCenter,
  Label,
  PillDatePicker,
  PillInput,
  PillSelect,
} from '../../components/CommonStyles';
import {
  MailOutlined,
  UserOutlined,
  PhoneOutlined,
  HomeOutlined,
  CalendarOutlined,
  CodeSandboxOutlined,
  FieldNumberOutlined,
} from '@ant-design/icons';
import { EmptyTableContent } from '../../components/EmptyTable';
import { useLoading } from '../../components/LoadingProvider';
import { useUser } from '../../components/UserProvider';
import { DetailOrder, SubOrderByUser, SuborderStatusText } from '../../interface/Order';
import { getAccessToken } from '../../utils/accessToken';
import { alertError } from '../../utils/alert';
// import { mockSenderSubOrderData } from '../../constants/mockTableData';
import { ReactComponent as WeightIcon } from '../../assets/weight.svg';

interface TableData extends SubOrderByUser, DetailOrder {
  key: string;
  status: string;
}

export const SenderSubOrders = () => {
  const { user } = useUser();
  const { setLoading } = useLoading();

  const [status, setStatus] = useState('None');
  const [orderFilter, setOrderFilter] = useState('');
  const [requestFilter, setRequestFilter] = useState('');
  const [fromDay, setFromDay] = useState(0);
  const [toDay, setToDay] = useState(Infinity);

  const [orders, setOrders] = useState<TableData[]>([]);

  useEffect(() => {
    const callGetOrderByUser = async () => {
      const payload = {
        userId: user.id,
        accessToken: getAccessToken(),
        reqTime: Date.now(),
      };

      try {
        setLoading(true);

        const { orders } = await getSubOrderByUser(payload);

        const ordersTableData = orders?.map((order) => {
          const detailOrder: DetailOrder = JSON.parse(order.detailOrder);

          return {
            ...order,
            ...detailOrder,
            key: order.subOrderId,
            status: SuborderStatusText[order.orderStatus],
          };
        });

        setOrders(ordersTableData);
        // setOrders(mockSenderSubOrderData);
      } catch (err) {
        alertError(err.message);
      } finally {
        setLoading(false);
      }
    };

    if (user) callGetOrderByUser();
  }, [setLoading, user]);

  const filterData = orders
    .filter((data) => {
      if (status === 'None') return true;
      return data.status === status;
    })
    .filter((data) => {
      if (orderFilter === '') return true;
      const subOrderId = data.subOrderId;
      return `${subOrderId}`.includes(orderFilter);
    })
    .filter((data) => {
      if (requestFilter === '') return true;
      const orderId = data.orderId;
      return `${orderId}`.includes(requestFilter);
    })
    .filter((data) => {
      return data.reqTime >= fromDay;
    })
    .filter((data) => {
      return data.reqTime <= toDay;
    });

  const filterStatus = (value: any) => {
    setStatus(value);
  };

  const onOrderFilterChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    setOrderFilter(e.target.value);
  };

  const onRequestFilterChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    setRequestFilter(e.target.value);
  };

  const onFromDayChange = (date: any) => {
    setFromDay(date ? startOfDay(date.valueOf()).getTime() : 0);
  };

  const onToDayChange = (date: any) => {
    setToDay(date ? endOfDay(date.valueOf()).getTime() : Infinity);
  };

  return (
    <>
      <FlexCenter>
        <FilterWrapper>
          <Label>Mã yêu cầu</Label>
          <PillInput
            onChange={onRequestFilterChange}
            placeholder="Lọc theo yêu cầu"
            value={requestFilter}
          />
        </FilterWrapper>

        <FilterWrapper>
          <Label>Mã đơn</Label>
          <PillInput
            onChange={onOrderFilterChange}
            placeholder="Lọc theo mã đơn"
            value={orderFilter}
          />
        </FilterWrapper>

        <FilterWrapper>
          <Label>Trạng thái</Label>
          <PillSelect defaultValue="None" onChange={filterStatus}>
            <Option value="None">None</Option>
            <Option value={SuborderStatusText[2]}>{SuborderStatusText[2]}</Option>
            <Option value={SuborderStatusText[3]}>{SuborderStatusText[3]}</Option>
            <Option value={SuborderStatusText[4]}>{SuborderStatusText[4]}</Option>
            <Option value={SuborderStatusText[5]}>{SuborderStatusText[5]}</Option>
            <Option value={SuborderStatusText[6]}>{SuborderStatusText[6]}</Option>
            <Option value={SuborderStatusText[7]}>{SuborderStatusText[7]}</Option>
          </PillSelect>
        </FilterWrapper>

        <FilterWrapper>
          <Label>Từ đầu ngày</Label>
          <PillDatePicker onChange={onFromDayChange} />
        </FilterWrapper>

        <FilterWrapper>
          <Label>Đến cuối ngày</Label>
          <PillDatePicker onChange={onToDayChange} />
        </FilterWrapper>
      </FlexCenter>
      <br />
      <Table
        bordered
        columns={orderColumns}
        dataSource={filterData}
        pagination={{ pageSize: 3, hideOnSinglePage: true }}
        locale={{ emptyText: <EmptyTableContent /> }}
      />
    </>
  );
};

const { Option } = PillSelect;

const orderColumns = [
  {
    title: 'Mã yêu cầu / đơn',
    render: (_: any, order: TableData) => {
      const { orderId, subOrderId, reqTime } = order;
      return (
        <BlueText>
          <p>Mã yêu cầu: {orderId}</p>
          <p>Mã đơn: {subOrderId}</p>
          <p>
            <CalendarOutlined /> Ngày tạo: {new Date(reqTime).toLocaleString()}
          </p>
        </BlueText>
      );
    },
  },
  {
    title: 'Bên nhận',
    render: (_: any, order: TableData) => {
      const { nameReceiver, toAddress, phoneReceiver, emailReceiver } = order;
      return (
        <BlueText>
          <p>
            <UserOutlined /> {nameReceiver}
          </p>
          <p>
            <PhoneOutlined /> {phoneReceiver}
          </p>
          <p>
            <MailOutlined /> {emailReceiver}
          </p>
          <p>
            <HomeOutlined /> {toAddress}
          </p>
        </BlueText>
      );
    },
  },
  {
    title: 'Hàng hóa',
    render: (_: any, order: TableData) => {
      const { weight, kind, totalWeight } = order;
      return (
        <BlueText>
          <p>
            <CodeSandboxOutlined /> {kind}
          </p>
          <p>
            <WeightIcon /> {weight}kg / {totalWeight}kg
          </p>
        </BlueText>
      );
    },
  },
  {
    title: 'Trạng thái đơn hàng',
    render: (_: any, order: TableData) => {
      const { status } = order;
      return <p style={{ color: 'red', fontWeight: 'bold' }}>{status}</p>;
    },
  },
  {
    title: 'Tài xế',
    render: (_: any, order: TableData) => {
      const {
        driverId,
        externalDriverId,
        destDriverId,
        driverName,
        driverPhone,
        destDriverName,
        destDriverPhone,
        externalDriverName,
        externalDriverPhone,
      } = order;

      return (
        <BlueText>
          {driverId ? (
            <div>
              <p style={{ color: 'red' }}>Tài nội tỉnh đi:</p>
              <p>
                <FieldNumberOutlined /> {driverId}
              </p>
              <p>
                <UserOutlined /> {driverName}
              </p>
              <p>
                <PhoneOutlined /> {driverPhone}
              </p>
            </div>
          ) : null}
          {externalDriverId ? (
            <div>
              <br />
              <p style={{ color: 'red' }}>Tài liên tỉnh:</p>
              <p>
                <FieldNumberOutlined /> {externalDriverId}
              </p>
              <p>
                <UserOutlined /> {externalDriverName}
              </p>
              <p>
                <PhoneOutlined /> {externalDriverPhone}
              </p>
            </div>
          ) : null}
          {destDriverId ? (
            <div>
              <br />
              <p style={{ color: 'red' }}>Tài nội tỉnh đến:</p>
              <p>
                <FieldNumberOutlined /> {destDriverId}
              </p>
              <p>
                <UserOutlined /> {destDriverName}
              </p>
              <p>
                <PhoneOutlined /> {destDriverPhone}
              </p>
            </div>
          ) : null}
        </BlueText>
      );
    },
  },
];
