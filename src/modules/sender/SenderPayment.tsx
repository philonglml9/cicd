import React, { useEffect, useState } from 'react';
import { Modal, Radio, Space, Steps, Table } from 'antd';

import { getOrderByUser } from '../../api/order';
import { getAccessToken } from '../../utils/accessToken';
import { useLoading } from '../../components/LoadingProvider';
import { useUser } from '../../components/UserProvider';
import { alertError } from '../../utils/alert';
import {
  OrderByUser,
  OrderStatus,
  PaymentStatus,
  OrderStatusText,
  DetailOrder,
} from '../../interface/Order';
// import { mockSenderRequestData } from '../../constants/mockTableData';
import {
  MailOutlined,
  UserOutlined,
  PhoneOutlined,
  HomeOutlined,
  CalendarOutlined,
  FieldNumberOutlined,
  CodeSandboxOutlined,
  DollarOutlined,
} from '@ant-design/icons';
import styled from 'styled-components';
import { payment } from '../../api/user';
import { PaymentMethod, PaymentPayload } from '../../interface/Auth';
import { BlueText, PillButton } from '../../components/CommonStyles';
import { EmptyTableContent } from '../../components/EmptyTable';
import { ReactComponent as WeightIcon } from '../../assets/weight.svg';

export const SenderPayment = () => {
  const { setLoading } = useLoading();
  const { user } = useUser();
  const [current, setCurrent] = React.useState(0);
  const [selectedRows, setSelectedRows] = React.useState<OrderByUser[]>([]);
  const [requests, setRequests] = useState<OrderByUser[]>([]);
  const [paymentMethod, setPaymentMethod] = React.useState<PaymentMethod>('ATM');

  const totalFee = selectedRows.reduce((accumulator, request) => accumulator + request.fee, 0);

  const onPaymentMethodChange = (e: any) => {
    setPaymentMethod(e.target.value);
  };

  const next = () => {
    setCurrent(current + 1);
  };

  const prev = () => {
    setCurrent(current - 1);
  };

  const rowSelection = {
    onChange: (_: any, selectedRows: OrderByUser[]) => {
      setSelectedRows(selectedRows);
    },
    selectedRowKeys: selectedRows.map((selectedRow) => selectedRow.orderId),
  };

  useEffect(() => {
    const callGetOrderByUser = async () => {
      try {
        const payload = {
          userId: user.id,
          accessToken: getAccessToken(),
          reqTime: Date.now(),
        };

        setLoading(true);

        const { orders } = await getOrderByUser(payload);

        const ordersTableData = orders
          ?.map((order) => {
            return {
              key: order.orderId,
              ...order,
              status: OrderStatusText[order.orderStatus],
            };
          })
          .filter(
            (order) =>
              order.orderStatus === OrderStatus.DONE &&
              order.paymentStatus === PaymentStatus.WAITING &&
              order.paymentSide === 'sender'
          );

        setRequests(ordersTableData);

        // const filteredMockSenderRequestData = mockSenderRequestData.filter(
        //   (order) =>
        //     order.orderStatus === OrderStatus.DONE &&
        //     order.paymentStatus === PaymentStatus.WAITING &&
        //     order.paymentSide === 'sender'
        // );
        // setRequests(filteredMockSenderRequestData);
      } catch (err) {
        alertError(err.message);
      } finally {
        setLoading(false);
      }
    };

    if (user) callGetOrderByUser();
  }, [setLoading, user]);

  const renderNextPrevButton = () => {
    return (
      <>
        {current > 0 && (
          <PillButton style={{ margin: '0 8px' }} onClick={() => prev()}>
            Previous
          </PillButton>
        )}
        {current < 3 - 1 && (
          <PillButton type="primary" onClick={() => next()} disabled={selectedRows.length === 0}>
            Next
          </PillButton>
        )}
      </>
    );
  };

  const onFinishPrompt = () => {
    Modal.confirm({
      title: <BlueText>Bạn có chắc muốn thanh toán?</BlueText>,
      onOk: onPayment,
    });
  };

  const onPayment = async () => {
    const description = 'Thanh toán đơn hàng #' + selectedRows.map((row) => row.orderId).join('#');

    const item = selectedRows.map((row) => ({
      id: row.orderId,
      fee: row.fee,
      weight: row.totalWeight,
      phoneReceiver: (JSON.parse(row.detailOrder) as DetailOrder).phoneReceiver,
    }));

    const codeOrders = JSON.stringify(selectedRows.map((row) => row.orderId));

    const payload: PaymentPayload = {
      typePayment: paymentMethod,
      amount: totalFee,
      description,
      item: JSON.stringify(item),
      embed_data: JSON.stringify({ promotioninfo: '', merchantinfo: '' }),
      codeOrders,
    };
    try {
      const res = await payment(payload);
      window.open(res.order_url);
    } catch (err) {
      alertError(err.message);
    }
  };

  return (
    <Container>
      <Steps current={current}>
        <Step title="Chọn yêu cầu muốn thanh toán" />
        <Step title="Chọn phương thức thanh toán" />
        <Step title="Xác nhận" />
      </Steps>
      <br />
      {renderNextPrevButton()}
      <br />
      <br />
      {current === 0 && (
        <Table
          bordered
          rowSelection={{
            type: 'checkbox',
            ...rowSelection,
          }}
          columns={requestColumns}
          dataSource={requests}
          locale={{ emptyText: <EmptyTableContent /> }}
        />
      )}
      {current === 1 && (
        <Radio.Group onChange={onPaymentMethodChange} value={paymentMethod}>
          <Space direction="vertical">
            <Radio value="ATM">ATM</Radio>
            <Radio value="CC">CC (Visa)</Radio>
            <Radio value="ZALOPAY">ZALOPAY (Ví)</Radio>
          </Space>
        </Radio.Group>
      )}
      {current === 2 && (
        <BlueText>
          <p>Tổng tiền thanh toán: {totalFee.toLocaleString()}</p>
          <p>Phương thức thanh toán: {paymentMethod}</p>
          <PillButton onClick={onFinishPrompt} type="primary">
            Xác nhận
          </PillButton>
        </BlueText>
      )}
    </Container>
  );
};

const requestColumns = [
  {
    title: 'Mã yêu cầu',
    render: (_: any, request: OrderByUser) => {
      const { orderId, orderTime } = request;
      return (
        <BlueText>
          <p>
            <FieldNumberOutlined /> {orderId}
          </p>
          <p>
            <CalendarOutlined /> {new Date(orderTime).toLocaleString()}
          </p>
        </BlueText>
      );
    },
  },
  {
    title: 'Bên nhận',
    render: (_: any, request: OrderByUser) => {
      const { nameReceiver, toAddress, phoneReceiver, emailReceiver }: DetailOrder = JSON.parse(
        request.detailOrder
      );
      return (
        <BlueText>
          <p>
            <UserOutlined /> {nameReceiver}
          </p>
          <p>
            <PhoneOutlined /> {phoneReceiver}
          </p>
          <p>
            <MailOutlined /> {emailReceiver}
          </p>
          <p>
            <HomeOutlined /> {toAddress}
          </p>
        </BlueText>
      );
    },
  },
  {
    title: 'Hàng hóa',
    render: (_: any, request: OrderByUser) => {
      const { fee, totalWeight, detailOrder } = request;
      const { kind }: DetailOrder = JSON.parse(detailOrder);

      return (
        <BlueText>
          <p>
            <CodeSandboxOutlined /> {kind}
          </p>
          <p>
            <WeightIcon /> {totalWeight.toLocaleString()} kg
          </p>

          <p style={{ color: 'red' }}>
            <DollarOutlined />{' '}
            {fee.toLocaleString('it-IT', {
              style: 'currency',
              currency: 'VND',
            })}
          </p>
        </BlueText>
      );
    },
  },
  {
    title: 'Thông tin thêm',
    render: (_: any, request: OrderByUser) => {
      const { description, value } = request;
      return (
        <BlueText>
          <p>Gía trị hàng hóa ước tính: {value.toLocaleString()}</p>
          <p>Ghi chú: {description}</p>
        </BlueText>
      );
    },
  },
];

const { Step } = Steps;

const Container = styled.div`
  margin: auto;
  max-width: 90%;
  margin-bottom: 100px;
  margin-top: 50px;
`;
