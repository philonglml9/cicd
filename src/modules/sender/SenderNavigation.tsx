import React, { ReactNode, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { Layout, Menu } from 'antd';
import {
  UserOutlined,
  UnorderedListOutlined,
  DollarOutlined,
  ExclamationCircleOutlined,
  LogoutOutlined,
} from '@ant-design/icons';
import {
  LayoutContent,
  Logo,
  User,
  UserIconWrapper,
  UserName,
} from '../../components/CommonStyles';
import { useGetUser } from '../../components/UserProvider';
import { ReactComponent as LogoImg } from '../../assets/logo.svg';
import userIcon from '../../assets/user.svg';

export const SenderNavigation = ({ children }: { children: ReactNode }) => {
  const { user } = useGetUser();
  const history = useHistory();
  const [selectedKey, setSelectedKey] = useState(
    history.location.pathname.split('/')[2]
  );

  const handleClick = (e: any) => {
    setSelectedKey(e.key);
    if (e.key === 'logout') {
      history.push(`/signin`);
    } else {
      history.push(`/sender/${e.key}`);
    }
  };

  useEffect(() => {
    setSelectedKey(history.location.pathname.split('/')[2]);
  }, [history.location.pathname]);

  return (
    <Layout style={{ height: '100%' }}>
      <Sider
        breakpoint="lg"
        collapsedWidth="0"
        style={{ backgroundColor: '#00467f' }}
      >
        <Logo onClick={() => history.push('/')}>
          <LogoImg />
        </Logo>

        <User>
          <UserIconWrapper>
            <img src={userIcon} alt="user" />
          </UserIconWrapper>
          <UserName>{user?.username}</UserName>
        </User>

        <Menu
          style={{ backgroundColor: '#00467f' }}
          theme="dark"
          mode="inline"
          onClick={handleClick}
          selectedKeys={[selectedKey]}
        >
          <Menu.Item key="account" icon={<UserOutlined />}>
            Chỉnh sửa thông tin
          </Menu.Item>
          <Menu.Item key="orders" icon={<UnorderedListOutlined />}>
            Quản lí đơn hàng
          </Menu.Item>
          <Menu.Item key="payment" icon={<DollarOutlined />}>
            Thanh toán
          </Menu.Item>
          <Menu.Item key="issue" icon={<ExclamationCircleOutlined />}>
            Yêu cầu hỗ trợ
          </Menu.Item>
          <Menu.Item key="logout" icon={<LogoutOutlined />}>
            Đăng xuất
          </Menu.Item>
        </Menu>
      </Sider>

      <Layout>
        <LayoutContent>{children}</LayoutContent>
      </Layout>
    </Layout>
  );
};

const { Sider } = Layout;
