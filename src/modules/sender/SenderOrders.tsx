import React from 'react';
import { Tabs } from 'antd';
import styled from 'styled-components';
import { useHistory } from 'react-router';
import { EditOutlined } from '@ant-design/icons';
import { SenderRequests } from './SenderRequests';
import { SenderSubOrders } from './SenderSubOrders';
import { PillButton } from '../../components/CommonStyles';
import { SenderCancel } from './SenderCancel';

export const SenderOrders = () => {
  const history = useHistory();

  const onTabChange = (key: any) => {
    history.push(key);
  };

  return (
    <Container>
      <PillButton type="primary" onClick={() => history.push(`/sender/new`)}>
        <EditOutlined />
        Lên đơn hàng
      </PillButton>
      <br />
      <Tabs defaultActiveKey={history.location.hash} centered onChange={onTabChange}>
        <TabPane tab="Yêu cầu lấy hàng" key="#1">
          <SenderRequests />
        </TabPane>
        <TabPane tab="Đơn hàng gửi" key="#2">
          <SenderSubOrders />
        </TabPane>
        <TabPane tab="Yêu cầu hủy" key="#3">
          <SenderCancel />
        </TabPane>
      </Tabs>
    </Container>
  );
};

const { TabPane } = Tabs;

const Container = styled.div`
  margin: auto;
  max-width: 90%;
  margin-bottom: 100px;
  margin-top: 25px;
`;
