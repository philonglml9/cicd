import { Button } from 'antd';
import { endOfDay, format, startOfDay } from 'date-fns';
import React, { useState } from 'react';
import styled from 'styled-components';
import { getStatisticFee, statisticTransRequest } from '../../api/order';
import { FilterWrapper, FlexCenter, Label, PillDatePicker } from '../../components/CommonStyles';
import { useLoading } from '../../components/LoadingProvider';
import { getAccessToken } from '../../utils/accessToken';
import { alertError, alertSuccess } from '../../utils/alert';
import { formatDate } from '../../utils/formatDate';
import { Line, Pie } from 'react-chartjs-2';
import { StatisticFeeObject, StatisticTransObject } from '../../interface/Order';

export const AdminStatistic = () => {
  const { setLoading } = useLoading();
  const [fromDate, setFromDate] = useState(0);
  const [toDate, setToDate] = useState(Infinity);
  const [statisticTransList, setStatisticTransList] = useState<StatisticTransObject[]>([]);
  const [statisticFees, setStatisticFees] = useState<StatisticFeeObject[]>([]);

  const onFromDayChange = (date: any) => {
    setFromDate(date ? startOfDay(date.valueOf()).getTime() : 0);
  };

  const onToDayChange = (date: any) => {
    setToDate(date ? endOfDay(date.valueOf()).getTime() : Infinity);
  };

  const labels = statisticTransList.map((item) => format(new Date(item.date), 'dd/MM'));
  const successData = statisticTransList.map((item) => item.transSuccess);
  const canceledData = statisticTransList.map((item) => item.transCancel);
  const failedData = statisticTransList.map((item) => item.transFail);
  const feeData = statisticFees.map((item) => item.totalFee);

  const totalFailed = failedData.reduce((item, acc) => acc + item, 0);
  const totalSuccess = successData.reduce((item, acc) => acc + item, 0);
  const totalCanceled = canceledData.reduce((item, acc) => acc + item, 0);

  const onClick = async () => {
    try {
      const payload = {
        reqTime: Date.now(),
        accessToken: getAccessToken(),
        fromDate: formatDate(fromDate),
        toDate: formatDate(toDate),
      };

      setLoading(true);
      const statistics = await Promise.all([
        getStatisticFee(payload),
        statisticTransRequest(payload),
      ]);
      const { statisticFees } = statistics[0];
      const { statisticTransList } = statistics[1];

      alertSuccess('Lấy thống kê thành công');
      setStatisticTransList(statisticTransList);
      setStatisticFees(statisticFees);
    } catch (err) {
      alertError(err.message);
    } finally {
      setLoading(false);
    }
  };

  const data: Chart.ChartData = {
    labels,
    datasets: [
      {
        label: 'Đơn hàng giao thành công',
        data: successData,
        backgroundColor: 'red',
        fill: false,
        lineTension: 0,
        borderColor: '#5cb85c',
      },
      {
        label: 'Đơn hàng hỏng / thất lạc',
        data: failedData,
        backgroundColor: 'blue',
        fill: false,
        lineTension: 0,
        borderColor: 'red',
      },
      {
        label: 'Đơn hàng hủy',
        data: canceledData,
        backgroundColor: 'yellow',
        fill: false,
        lineTension: 0,
        borderColor: 'pink',
      },
    ],
  };

  const pieData: Chart.ChartData = {
    labels: ['Đơn hàng giao thành công', 'Đơn hàng hỏng / thất lạc', 'Đơn hàng hủy'],
    datasets: [
      {
        data: [totalSuccess, totalFailed, totalCanceled],
        backgroundColor: ['#5cb85c', 'red', 'pink'],
        borderWidth: 1,
      },
    ],
  };

  const feeChartData: Chart.ChartData = {
    labels,
    datasets: [
      {
        label: 'Phí người dùng đã thanh toán',
        data: feeData,
        backgroundColor: 'red',
        fill: false,
        lineTension: 0,
        borderColor: '#5cb85c',
      },
    ],
  };

  return (
    <Container>
      <FlexCenter>
        <FilterWrapper>
          <Label>Từ đầu ngày</Label>
          <PillDatePicker onChange={onFromDayChange} />
        </FilterWrapper>

        <FilterWrapper>
          <Label>Đến cuối ngày</Label>
          <PillDatePicker onChange={onToDayChange} />
        </FilterWrapper>

        <ButtonWrapper>
          <Button onClick={onClick} type="primary">
            Search
          </Button>
        </ButtonWrapper>
      </FlexCenter>
      <br />
      <br />
      <ChartContainer>
        <ChartWrapper>
          <Line data={data} options={options} />
        </ChartWrapper>
        <ChartWrapper>
          <Pie data={pieData} options={pieChartOptions} />
        </ChartWrapper>
      </ChartContainer>
      <br />
      <br />
      <ChartContainer>
        <ChartWrapper>
          <Line data={feeChartData} options={options2} />
        </ChartWrapper>
        <ChartWrapper style={{ visibility: 'hidden' }}>
          <Pie data={pieData} options={pieChartOptions} />
        </ChartWrapper>
      </ChartContainer>
    </Container>
  );
};

const ButtonWrapper = styled.div`
  display: flex;
  align-items: flex-end;
`;

const Container = styled.div`
  margin: auto;
  max-width: 80%;
  margin-bottom: 100px;
  margin-top: 20px;
`;

const options: Chart.ChartOptions = {
  scales: {
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
          stepSize: 10,
        },
      },
    ],
  },
};

const options2: Chart.ChartOptions = {
  scales: {
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
        },
      },
    ],
  },
};

const pieChartOptions: Chart.ChartOptions = {
  legend: {
    display: false,
  },
};

const ChartContainer = styled.div`
  display: flex;
  justify-content: space-around;
`;

const ChartWrapper = styled.div`
  width: 100%;
  height: 100%;
`;
