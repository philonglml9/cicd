import React, { ReactNode, useState } from 'react';
import { useHistory } from 'react-router';
import { Layout, Menu } from 'antd';
import {
  LayoutContent,
  Logo,
  User,
  UserIconWrapper,
  UserName,
} from '../../components/CommonStyles';
import { useGetUser } from '../../components/UserProvider';
import { ReactComponent as LogoImg } from '../../assets/logo.svg';
import userIcon from '../../assets/user.svg';

export const AdminNavigation = ({ children }: { children: ReactNode }) => {
  const { user } = useGetUser();
  const [, forceUpdate] = useState('');
  const history = useHistory();

  const handleClick = (e: any) => {
    forceUpdate(e.key);
    if (e.key === 'logout') {
      history.push(`/signin`);
    } else {
      history.push(`/admin/${e.key}`);
    }
  };

  return (
    <Layout style={{ height: '100%' }}>
      <Sider
        style={{ backgroundColor: '#00467f' }}
        breakpoint="lg"
        collapsedWidth="0"
      >
        <Logo onClick={() => history.push('/')}>
          <LogoImg />
        </Logo>

        <User>
          <UserIconWrapper>
            <img src={userIcon} alt="user" />
          </UserIconWrapper>
          <UserName>{user?.username}</UserName>
        </User>

        <Menu
          style={{ backgroundColor: '#00467f' }}
          theme="dark"
          mode="inline"
          onClick={handleClick}
          selectedKeys={[history.location.pathname.split('/')[2]]}
        >
          <Menu.Item key="assign">Thông tin nội bộ</Menu.Item>
          <Menu.Item key="statistic">Thống kê</Menu.Item>
          <Menu.Item key="addStock">Thêm kho</Menu.Item>
          <Menu.Item key="addStockkeeper">Thêm thủ kho</Menu.Item>
          <Menu.Item key="addDriver">Thêm tài xế</Menu.Item>
          <Menu.Item key="issue">Yêu cầu hỗ trợ</Menu.Item>
          <Menu.Item key="logout">Đăng xuất</Menu.Item>
        </Menu>
      </Sider>

      <Layout>
        <LayoutContent>{children}</LayoutContent>
      </Layout>
    </Layout>
  );
};

const { Sider } = Layout;
