/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect } from 'react';
import { Modal, Table } from 'antd';
import { useLoading } from '../../components/LoadingProvider';
import { getAccessToken } from '../../utils/accessToken';
import {
  getSubOrderByDriver,
  // sendSuborderIssue,
  updateSubOrder,
} from '../../api/order';
import { alertError, alertSuccess } from '../../utils/alert';
import { DetailOrder, SubOrderByDriver, SuborderStatus } from '../../interface/Order';
import {
  BlueText,
  FlexSpaceBetween,
  InfoContainer,
  InfoTitle,
  // PillButton,
  PillInputNumber,
} from '../../components/CommonStyles';
import { useUser } from '../../components/UserProvider';
import { useHistory } from 'react-router';
import { EmptyTableContent } from '../../components/EmptyTable';
// import { mockSubOrderByDriver } from '../../constants/mockTableData';
import { UserOutlined, PhoneOutlined, HomeOutlined, CodeSandboxOutlined } from '@ant-design/icons';
import { ReactComponent as WeightIcon } from '../../assets/weight.svg';
import TextArea from 'antd/lib/input/TextArea';
import ReactGA from 'react-ga';

export const DriverCurrentOrder = () => {
  const { setLoading } = useLoading();
  const history = useHistory();

  const [orders, setOrders] = useState<SubOrderByDriver[]>(null);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isIssueModalVisible, setIsIssueModalVisible] = useState(false);
  const [selectedOrder, setSelectedOrder] = useState<SubOrderByDriver>(null);
  const [weightToTake, setWeightToTake] = useState(null);

  const { user } = useUser();
  const [subOrders, setSubOrders] = useState<SubOrderByDriver[]>(null);

  const [issue, setIssue] = useState('');

  useEffect(() => {
    const callGetSubOrderByDriver = async () => {
      const payload = {
        driverId: user.id,
        accessToken: getAccessToken(),
        reqTime: Date.now(),
      };
      try {
        setLoading(true);
        const { orders } = await getSubOrderByDriver(payload);
        setSubOrders(orders);
        // setSubOrders(mockSubOrderByDriver);
      } catch (err) {
        alertError(err.message);
      } finally {
        setLoading(false);
      }
    };

    if (user?.id && history.location.hash === '#2') callGetSubOrderByDriver();
  }, [setLoading, user?.id, history.location.hash]);

  useEffect(() => {
    const ordersTableData = subOrders
      ?.filter((subOrder) => subOrder.orderStatus === SuborderStatus.REQUESTED)
      .map((order) => {
        return {
          key: order.subOrderId,
          ...order,
        };
      });
    setOrders(ordersTableData);
  }, [subOrders]);

  const onTextAreaChange = (e: any) => {
    setIssue(e.target.value);
  };

  const requestColumns = [
    {
      title: 'Mã yêu cầu / đơn	',
      render: (_: any, order: SubOrderByDriver) => {
        const { subOrderId, orderId } = order;
        return (
          <BlueText>
            <p>
              Mã đơn: <a onClick={() => onOrderClick(order)}>{subOrderId}</a>
            </p>
            <p>Mã yêu cầu: {orderId}</p>
          </BlueText>
        );
      },
    },
    {
      title: 'Thông tin khách hàng',
      render: (_: any, order: SubOrderByDriver) => {
        const { nameSender, fromAddress, phoneSender }: DetailOrder = JSON.parse(order.detailOrder);
        return (
          <BlueText>
            <p>
              <UserOutlined /> {nameSender}
            </p>
            <p>
              <PhoneOutlined /> {phoneSender}
            </p>
            <p>
              <HomeOutlined /> {fromAddress}
            </p>
          </BlueText>
        );
      },
    },
    {
      title: 'Hàng hóa',
      render: (_: any, order: SubOrderByDriver) => {
        const { remainWeight, totalWeight } = order;
        const { kind }: DetailOrder = JSON.parse(order.detailOrder);
        return (
          <BlueText>
            <p>
              <CodeSandboxOutlined /> {kind}
            </p>
            <p>
              <WeightIcon /> Tổng khối lượng: {totalWeight}kg
            </p>
            <p style={{ color: 'red' }}>Còn lại: {remainWeight}kg</p>
          </BlueText>
        );
      },
    },
    // {
    //   title: 'Báo cáo vấn đề',
    //   render: (_: any, order: SubOrderByDriver) => {
    //     return (
    //       <PillButton onClick={() => onIssueClick(order)} danger type="primary">
    //         Báo cáo
    //       </PillButton>
    //     );
    //   },
    // },
  ];

  // const onIssueClick = (subOrder: SubOrderByDriver) => {
  //   setIsIssueModalVisible(true);
  //   setSelectedOrder(subOrder);
  // };

  const onOrderClick = (order: SubOrderByDriver) => {
    setSelectedOrder(order);
    setWeightToTake(order.remainWeight);
    setIsModalVisible(true);
  };

  const onInputChange = (value: number) => {
    setWeightToTake(value);
  };

  const handleOk = async () => {
    const payload = {
      actualWeight: weightToTake,
      subOrderId: selectedOrder.subOrderId,
      accessToken: getAccessToken(),
      reqTime: Date.now(),
    };

    try {
      setLoading(true);
      await updateSubOrder(payload);
      ReactGA.event({
        category: 'Driver',
        action: 'Driver update suborder to truck',
      });
      setOrders([]);
      alertSuccess('Chấp nhận đơn hàng thành công');
    } catch (err) {
      alertError(err.message);
    } finally {
      setLoading(false);
    }
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const renderModal = () => {
    if (!selectedOrder) return null;

    const { kind, phoneSender, nameSender, fromAddress }: DetailOrder = JSON.parse(
      selectedOrder.detailOrder
    );

    return (
      <Modal
        width={800}
        title="Thông tin đơn hàng"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        okText="Chấp nhận"
      >
        <FlexSpaceBetween>
          <div>
            <InfoTitle>| Khách hàng</InfoTitle>
            <InfoContainer>
              <p>Số điện thoại: {phoneSender}</p>
              <p>Họ tên: {nameSender}</p>
              <p>Địa chỉ: {fromAddress}</p>
            </InfoContainer>
          </div>

          <div>
            <InfoTitle>| Hàng hoá</InfoTitle>
            <InfoContainer>
              <p>Loại hàng: {kind}</p>
              <p>Khối lượng tổng: {selectedOrder.totalWeight}</p>
              <span>Khối lượng lấy: </span>
              <PillInputNumber value={weightToTake} onChange={onInputChange} />
            </InfoContainer>
          </div>
        </FlexSpaceBetween>
      </Modal>
    );
  };

  const renderIssueModal = () => {
    return (
      <Modal
        title="Báo cáo sự cố"
        visible={isIssueModalVisible}
        onOk={sendIssue}
        okButtonProps={{ danger: true }}
        onCancel={() => setIsIssueModalVisible(false)}
        okText="Gửi báo cáo"
      >
        <TextArea
          value={issue}
          onChange={onTextAreaChange}
          rows={5}
          placeholder="Ghi vấn đề gặp phải"
        />
      </Modal>
    );
  };

  const sendIssue = async () => {
    const payload = {
      reqTime: Date.now(),
      accessToken: getAccessToken(),
      subOrderId: selectedOrder?.subOrderId,
      driverId: user.id,
      issue,
    };
    console.log('🚀 ~ payload', payload);
    // try {
    //   setLoading(true);
    //   await sendSuborderIssue(payload);
    //   alertSuccess('Xóa yêu cầu thành công');
    //   setSelectedOrder(null);
    //   setIsIssueModalVisible(false);
    //   setOrders([]);
    // } catch (err) {
    //   alertError(err.message);
    // } finally {
    //   setLoading(false);
    // }
  };

  return (
    <>
      <Table
        bordered
        columns={requestColumns}
        dataSource={orders}
        pagination={{ pageSize: 3, hideOnSinglePage: true }}
        locale={{ emptyText: <EmptyTableContent /> }}
      />

      {renderModal()}
      {renderIssueModal()}
    </>
  );
};
