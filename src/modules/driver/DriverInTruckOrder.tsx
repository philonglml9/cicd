/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect } from 'react';
import { Table, Modal } from 'antd';
import {
  DetailOrder,
  SubOrderByDriver,
  SuborderStatus,
} from '../../interface/Order';
import {
  FlexSpaceBetween,
  InfoTitle,
  InfoContainer,
  BlueText,
  PillButton,
} from '../../components/CommonStyles';
import {
  getSubOrderByDriver,
  sendSuborderIssue,
  updateSubOrder,
} from '../../api/order';
import { useLoading } from '../../components/LoadingProvider';
import { getAccessToken } from '../../utils/accessToken';
import { alertSuccess, alertError } from '../../utils/alert';
import { useUser } from '../../components/UserProvider';
import { useHistory } from 'react-router';
import { EmptyTableContent } from '../../components/EmptyTable';
// import { mockSubOrderByDriver } from '../../constants/mockTableData';
import {
  UserOutlined,
  PhoneOutlined,
  HomeOutlined,
  CodeSandboxOutlined,
} from '@ant-design/icons';
import TextArea from 'antd/lib/input/TextArea';

export const DriverInTruckOrder = () => {
  const { setLoading } = useLoading();
  const history = useHistory();
  const [orders, setOrders] = useState<SubOrderByDriver[]>(null);
  const [selectedOrder, setSelectedOrder] = useState<SubOrderByDriver>(null);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isIssueModalVisible, setIsIssueModalVisible] = useState(false);

  const { user } = useUser();
  const [subOrders, setSubOrders] = useState<SubOrderByDriver[]>(null);

  const [issue, setIssue] = useState('');

  useEffect(() => {
    const callGetSubOrderByDriver = async () => {
      const payload = {
        driverId: user.id,
        accessToken: getAccessToken(),
        reqTime: Date.now(),
      };
      try {
        setLoading(true);
        const { orders } = await getSubOrderByDriver(payload);
        setSubOrders(orders);
        // setSubOrders(mockSubOrderByDriver);
      } catch (err) {
        alertError(err.message);
      } finally {
        setLoading(false);
      }
    };

    if (user?.id && history.location.hash === '#3') {
      callGetSubOrderByDriver();
    }
  }, [setLoading, user?.id, history.location.hash]);

  const onOrderClick = (order: SubOrderByDriver) => {
    setSelectedOrder(order);
    setIsModalVisible(true);
  };

  const handleOk = async () => {
    const payload = {
      actualWeight: selectedOrder.weight,
      subOrderId: selectedOrder.subOrderId,
      accessToken: getAccessToken(),
      reqTime: Date.now(),
    };

    try {
      setLoading(true);
      await updateSubOrder(payload);
      setOrders(orders.filter((order) => order !== selectedOrder));
      alertSuccess('Giao đơn hàng thành công');
    } catch (err) {
      alertError(err.message);
    } finally {
      setLoading(false);
    }
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  useEffect(() => {
    const ordersTableData = subOrders
      ?.filter(
        (subOrder) =>
          subOrder.orderStatus === SuborderStatus.COMING_SEND_STOCK ||
          subOrder.orderStatus === SuborderStatus.TRANSFERING_TO_RECEIVER
      )
      .map((order) => ({
        key: order.subOrderId,
        ...order,
      }));
    setOrders(ordersTableData);
  }, [subOrders]);

  const requestColumns = [
    {
      title: 'Mã yêu cầu / đơn',
      render: (_: any, order: SubOrderByDriver) => {
        const { subOrderId, orderId, orderStatus } = order;
        return (
          <BlueText>
            <p>Mã đơn: {subOrderId}</p>
            <p>Mã yêu cầu: {orderId}</p>
            <PillButton
              onClick={() => onIssueClick(order)}
              danger
              type="primary"
            >
              Báo cáo sự cố
            </PillButton>
            <br />
            <br />
            {orderStatus === SuborderStatus.TRANSFERING_TO_RECEIVER && (
              <PillButton type="primary" onClick={() => onOrderClick(order)}>
                Hoàn tất giao hàng
              </PillButton>
            )}
          </BlueText>
        );
      },
    },
    {
      title: 'Thông tin người gửi',
      render: (_: any, order: SubOrderByDriver) => {
        const { nameSender, fromAddress, phoneSender }: DetailOrder =
          JSON.parse(order.detailOrder);

        return (
          <BlueText>
            <p>
              <UserOutlined /> {nameSender}
            </p>
            <p>
              <PhoneOutlined /> {phoneSender}
            </p>
            <p>
              <HomeOutlined /> {fromAddress}
            </p>
          </BlueText>
        );
      },
    },
    {
      title: 'Thông tin người nhận',
      render: (_: any, order: SubOrderByDriver) => {
        const { nameReceiver, toAddress, phoneReceiver }: DetailOrder =
          JSON.parse(order.detailOrder);

        return (
          <BlueText>
            <p>
              <UserOutlined /> {nameReceiver}
            </p>
            <p>
              <PhoneOutlined /> {phoneReceiver}
            </p>
            <p>
              <HomeOutlined /> {toAddress}
            </p>
          </BlueText>
        );
      },
    },
    {
      title: 'Hàng hóa',
      render: (_: any, order: SubOrderByDriver) => {
        const { weight, totalWeight, detailOrder } = order;
        const { kind }: DetailOrder = JSON.parse(detailOrder);
        return (
          <BlueText>
            <p>
              <CodeSandboxOutlined /> {kind}
            </p>
            <p style={{ color: 'red' }}>
              Khối lượng: {weight.toLocaleString()}/
              {totalWeight.toLocaleString()}kg
            </p>
          </BlueText>
        );
      },
    },
  ];

  const onIssueClick = (subOrder: SubOrderByDriver) => {
    setIsIssueModalVisible(true);
    setSelectedOrder(subOrder);
  };

  const sendIssue = async () => {
    const payload = {
      reqTime: Date.now(),
      accessToken: getAccessToken(),
      subOrderId: selectedOrder?.subOrderId,
      driverId: user.id,
      issue,
    };
    try {
      setLoading(true);
      await sendSuborderIssue(payload);
      alertSuccess('Gửi báo cáo thành công');
      setOrders(orders.filter((order) => order !== selectedOrder));
      setSelectedOrder(null);
      setIsIssueModalVisible(false);
    } catch (err) {
      alertError(err.message);
    } finally {
      setLoading(false);
    }
  };

  const renderIssueModal = () => {
    return (
      <Modal
        title="Báo cáo sự cố"
        visible={isIssueModalVisible}
        onOk={sendIssue}
        okButtonProps={{ danger: true }}
        onCancel={() => setIsIssueModalVisible(false)}
        okText="Gửi báo cáo"
      >
        <TextArea
          value={issue}
          onChange={onTextAreaChange}
          rows={5}
          placeholder="Ghi vấn đề gặp phải"
        />
      </Modal>
    );
  };

  const onTextAreaChange = (e: any) => {
    setIssue(e.target.value);
  };

  const renderModal = () => {
    if (!selectedOrder) return null;

    const {
      nameReceiver,
      toAddress,
      phoneReceiver,
      phoneSender,
      nameSender,
      fromAddress,
      kind,
    }: DetailOrder = JSON.parse(selectedOrder.detailOrder);

    return (
      <Modal
        width={700}
        title="Thông tin đơn hàng"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        okText="Chấp nhận"
      >
        <FlexSpaceBetween>
          <div>
            <InfoTitle>| Bên nhận</InfoTitle>
            <InfoContainer>
              <p>Số điện thoại: {phoneSender}</p>
              <p>Họ tên: {nameSender}</p>
              <p>Địa chỉ: {fromAddress}</p>
            </InfoContainer>
          </div>

          <div>
            <InfoTitle>| Hàng hoá</InfoTitle>
            <InfoContainer>
              <p>Loại hàng: {kind}</p>
              <span>Khối lượng: {selectedOrder.weight}</span>
            </InfoContainer>
          </div>
        </FlexSpaceBetween>
        <br />
        <div>
          <InfoTitle>| Bên gửi</InfoTitle>
          <InfoContainer>
            <p>Số điện thoại: {phoneReceiver}</p>
            <p>Họ tên: {nameReceiver}</p>
            <p>Địa chỉ: {toAddress}</p>
          </InfoContainer>
        </div>
      </Modal>
    );
  };

  return (
    <>
      <Table
        bordered
        columns={requestColumns}
        dataSource={orders}
        pagination={{ pageSize: 3, hideOnSinglePage: true }}
        locale={{ emptyText: <EmptyTableContent /> }}
      />

      {renderModal()}
      {renderIssueModal()}
    </>
  );
};
