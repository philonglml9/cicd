/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from 'react';
import { Tabs, Switch } from 'antd';
import styled from 'styled-components';
import { DriverFindOrder } from './DriverFindOrder';
import { useLoading } from '../../components/LoadingProvider';
import { updateStatusDriver } from '../../api/user';
import { useUser } from '../../components/UserProvider';
import { DriverInterface } from '../../interface/Auth';
import { alertError } from '../../utils/alert';
import { DriverCurrentOrder } from './DriverCurrentOrder';
import { DriverInTruckOrder } from './DriverInTruckOrder';
import { useHistory } from 'react-router';

export const DriverOrders = () => {
  const history = useHistory();
  const { user, setUser } = useUser();
  const { setLoading } = useLoading();

  const [isActive, setIsActive] = useState(!!(user as DriverInterface)?.status);

  useEffect(() => {
    setIsActive(!!(user as DriverInterface)?.status);
  }, [user]);

  const onSwitchChange = async () => {
    try {
      setLoading(true);
      const status = isActive ? 0 : 1;
      await updateStatusDriver({ status });
      setUser({ ...user, status });
    } catch (err) {
      alertError(err.message);
    } finally {
      setLoading(false);
    }
  };

  const onTabChange = (key: any) => {
    history.push(key);
  };

  return (
    <Container>
      <Switch checked={isActive} onChange={onSwitchChange} />
      <Tabs
        defaultActiveKey={history.location.hash}
        centered
        onChange={onTabChange}
      >
        <TabPane tab="Tìm đơn" key="#1">
          {isActive ? (
            <DriverFindOrder />
          ) : (
            'Hãy chuyển sang trạng thái hoạt động để nhận đơn'
          )}
        </TabPane>
        <TabPane tab="Yêu cầu đang thực hiện" key="#2">
          <DriverCurrentOrder />
        </TabPane>
        <TabPane tab="Đơn đang có trong xe" key="#3">
          <DriverInTruckOrder />
        </TabPane>
      </Tabs>
    </Container>
  );
};

const { TabPane } = Tabs;

const Container = styled.div`
  margin: auto;
  max-width: 90%;
  margin-bottom: 100px;
  margin-top: 50px;
`;
