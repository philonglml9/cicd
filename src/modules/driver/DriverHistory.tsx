import React, { useEffect, useState } from 'react';
import { Table } from 'antd';
import styled from 'styled-components';
import { BlueText } from '../../components/CommonStyles';
import { getAccessToken } from '../../utils/accessToken';
import { useUser } from '../../components/UserProvider';
import { getDriverHistoryRequest } from '../../api/order';
import { alertError } from '../../utils/alert';
import { useLoading } from '../../components/LoadingProvider';
import { EmptyTableContent } from '../../components/EmptyTable';
import { DetailOrder, SubOrderResponse } from '../../interface/Order';
import {
  MailOutlined,
  UserOutlined,
  PhoneOutlined,
  HomeOutlined,
  CalendarOutlined,
  CodeSandboxOutlined,
} from '@ant-design/icons';
import { ReactComponent as WeightIcon } from '../../assets/weight.svg';

export const DriverHistory = () => {
  const { user } = useUser();
  const { setLoading } = useLoading();
  const [subOrders, setSubOrders] = useState<SubOrderResponse[]>([]);

  useEffect(() => {
    const callGetDriverHistoryRequest = async () => {
      const payload = {
        reqTime: Date.now(),
        accessToken: getAccessToken(),
        driverId: user.id,
      };

      try {
        setLoading(true);
        const { subOrderResponses } = await getDriverHistoryRequest(payload);
        const tableSubOrderResponses = subOrderResponses.map((item) => ({
          ...item,
          key: item.subOrderId,
        }));
        setSubOrders(tableSubOrderResponses);
      } catch (err) {
        alertError(err.message);
      } finally {
        setLoading(false);
      }
    };

    if (user) callGetDriverHistoryRequest();
  }, [setLoading, user]);

  return (
    <Container>
      <BlueText style={{ textAlign: 'center', fontSize: '20px' }}>
        Lịch sử giao nhận
      </BlueText>
      <br />
      <Table
        locale={{ emptyText: <EmptyTableContent /> }}
        bordered
        columns={columns}
        dataSource={subOrders}
      />
    </Container>
  );
};

const Container = styled.div`
  margin: auto;
  max-width: 90%;
  margin-bottom: 100px;
  margin-top: 100px;
`;

const columns = [
  {
    title: 'ID',
    render: (_: any, order: SubOrderResponse) => {
      const { subOrderId, reqTime, orderId } = order;
      return (
        <BlueText>
          <p>Mã yêu cầu: {orderId}</p>
          <p>Mã đơn: {subOrderId}</p>
          <p>
            <CalendarOutlined /> {new Date(reqTime).toLocaleString()}
          </p>
        </BlueText>
      );
    },
  },
  {
    title: 'Người gửi',
    render: (_: any, order: SubOrderResponse) => {
      const { fromAddress, nameSender, phoneSender, emailSender }: DetailOrder =
        JSON.parse(order.detailOrder);
      return (
        <BlueText>
          <p>
            <UserOutlined /> {nameSender}
          </p>
          <p>
            <PhoneOutlined /> {phoneSender}
          </p>
          <p>
            <MailOutlined /> {emailSender}
          </p>
          <p>
            <HomeOutlined /> {fromAddress}
          </p>
        </BlueText>
      );
    },
  },
  {
    title: 'Người nhận',
    render: (_: any, order: SubOrderResponse) => {
      const {
        toAddress,
        nameReceiver,
        phoneReceiver,
        emailReceiver,
      }: DetailOrder = JSON.parse(order.detailOrder);
      return (
        <BlueText>
          <p>
            <UserOutlined /> {nameReceiver}
          </p>
          <p>
            <PhoneOutlined /> {phoneReceiver}
          </p>
          <p>
            <MailOutlined /> {emailReceiver}
          </p>
          <p>
            <HomeOutlined /> {toAddress}
          </p>
        </BlueText>
      );
    },
  },
  {
    title: 'Hàng hóa',
    render: (_: any, order: SubOrderResponse) => {
      const { kind }: DetailOrder = JSON.parse(order.detailOrder);
      const { weight } = order;
      return (
        <BlueText>
          <p>
            <CodeSandboxOutlined /> {kind}
          </p>
          <p>
            <WeightIcon /> {weight}kg
          </p>
        </BlueText>
      );
    },
  },
];
